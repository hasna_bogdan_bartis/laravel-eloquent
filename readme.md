# Laravel Eloquent Extension

## Installation

Add the following to your `composer.json`:

```json
{
    "require": {
        "hasna/laravel-eloquent": "1.0"
    },
    "repositories": [
        {
            "type": "vcs",
            "url": "git@bitbucket.org:hasna_bogdan_bartis/laravel-eloquent.git"
        }
    ]
}
```

## Usage

To be able to use the `sync` method on a `hasMany()` relation, just extend the model class like so:

```php
use Hasna\Eloquent\Model;

class Server extends Model
{
    public function ips()
    {
        return $this->hasMany(Ip::class);
    }
}
```

Then sync your code like so:

```php
$ips = [
    [
        'id' => 1,
        'ip' => '192.168.0.100'
    ],
    [
        'id' => null,
        'ip' => '192.168.0.101'
    ],
    [
        'ip' => '192.168.0.102'
    ],
];

Server::find(1)->ips()->sync($ips);
```

The `sync` method accepts an array of items as key value pairs. If the relation identifier is `null` or not present, a
new item will be created.
