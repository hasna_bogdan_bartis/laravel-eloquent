<?php

namespace Hasna\Eloquent\Relations;

use Illuminate\Database\Eloquent\Relations\HasMany as BaseHasMany;

class HasMany extends BaseHasMany
{
    /**
     * Sync the relationship entries.
     *
     * @param array $data
     *
     * @return array
     */
    public function sync(array $data)
    {
        $changes = [
            'created' => [],
            'deleted' => [],
            'updated' => []
        ];

        $current = $this->newQuery()->pluck(
            $this->localKey
        )->all();

        $create = [];
        $update = [];
        $delete = [];

        foreach ($data as $item) {
            $id = $item[$this->localKey] ?? null;

            if (!empty($id) && in_array($id, $current)) {
                $update[$id] = $item;
            } else {
                $create[] = $item;
            }
        }

        $changes['updated'] = array_keys($update);

        foreach ($current as $id) {
            if (!in_array($id, $changes['updated'])) {
                $delete[] = $id;
            }
        }

        // Delete items
        if (count($delete) > 0) {
            $this->getRelated()->destroy($delete);

            $changes['deleted'] = $delete;
        }

        // Update items
        foreach ($update as $id => $item) {
            $this->getRelated()->where($this->localKey, $id)->update($item);
        }

        // Create items
        foreach ($create as $item) {
            $changes['created'][] = $this->create($item)->{$this->localKey};
        }

        return $changes;
    }
}
